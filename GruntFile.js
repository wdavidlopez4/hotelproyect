module.exports = function(grunt)
{
    //estadisticas de ejecucion
    require('time-grunt')(grunt);

    //agilizar la ejecucion
    require('jit-grunt')(grunt, {useminPrepare: 'grunt-usemin'});


    grunt.initConfig(
        {   
            //tarea para renderizar el sass en css
            sass: 
            {
                dist:
                {
                    files:
                    [{
                        expand: true,
                        cwd: 'css',
                        src:['*.scss'],
                        dest: 'css',
                        ext: '.css'
                    }]
                }
            },

            //tarea para monitoriar los archivos css y si hay cambios luego ejecutar la tarea sass que renderiza
            watch:
            {
                files: ['css/*.scss'],
                tasks: ['css']
            },

            //tarea que esta pendiente de los cambios de los archivos css, html, js para actualizar el brower deimediato
            browserSync: 
            {
                dev:
                {
                    bsFiles:
                    {
                        src: ['css/*.css','*.html','js/*.js']
                    },

                    options: 
                    {
                        watchTask: true,
                        server: 
                        {
                            baseDir: './'
                        }
                    }
                }
            },

            //aqui comienzan las herramientas de desplieque

            //minimizar imagenes
            imagemin:
            {
                dynamic:
                {
                    files:
                    [{
                        expand: true,
                        cwd: './',
                        src: 'images/*.{png,gif,jpg,jpeg,PNG}',
                        dest: 'dist/'
                    }]
                }
            },

            //copiar archivos
            copy:
            {
                html:
                {
                    files: 
                    [{
                        expand: true,
                        dot: true,
                        cwd: './',
                        src: ['*.html'],
                        dest: 'dist'
                    }]
                },
                    //copia los iconos de la open-iconic
                fonts:
                {
                    files:
                    [{
                        expand: true,
                        dot: true,
                        cwd: 'node_modules/open-iconic/font',
                        src: ['fonts/*.*'],
                        dest: 'dist'
                    }]
                }
            },

            //limpiar archivos
            clean:
            {
                build:
                {
                    src: ['dist/']
                }
            },

            //concatenar archivos
            concat:
            {
                options:
                {
                    separator: ';'
                },
                dist:{}
            },

            //minificar archivos
            cssmin:
            {
                dist:{}
            },

            //ofuscar archivos
            uglify:
            {
                dist:{}
            },

            //versionar archivos
            filerev:
            {
                options:
                {
                    encoding: 'utf8',
                    algorithm: 'md5',
                    length: 20
                },

                release:
                {
                    files: 
                    [{
                        src:['dist/js/*.js', 'dist/css/*.css']
                    }]
                }
            },

            //preparar para ofuscar y minimizar
            useminPrepare:
            {
                foo:
                {
                    dest: 'dist',
                    src: ['index.html', 'precio.html', 'contact.html', 'about.html']
                },

                options:
                {
                    flow:
                    {
                        steps:
                        {
                            css: ['cssmin'],
                            js: ['uglify']
                        },

                        post:
                        {
                            css:
                            [{
                                name: 'cssmin',
                                createConfig: function(context, block)
                                {
                                    var generated = context.options.generated;
                                    generated.options = {
                                        keepSpecialComments: 0,
                                        release: false
                                    }
                                }
                            }]
                        }
                    }
                }
            },

            //selecciona los script que tiene cada html y los vuelve uno solo
            usemin:
            {
                html: ['dist/index.html', 'dist/precio.html', 'dist/contact.html', 'dist/about.html'],
                options:
                {
                    assetsDir: ['dist', 'dist/css', 'dist/js']
                }
            }




        }
    );

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    
    
    //tarea que ejecuta todas las herramientas para el desplieque de la aplicacion
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);
}