$(function () {
    //para activar el tooltip
    $('[data-toggle="tooltip"]').tooltip()

    //para anviar la transicion del carusel
    $('.carousel').carousel({ interval: 3000 })

    //colbac, nos suscribimos al evento "show.bs.modal" para que nos informe si se esta mostrando el modal, el evento "show.bs.modal" pertenece a el componenete modal
    $('#Confirmacion').on('show.bs.modal', function (e) {
      console.log("se esta mostrando el modal")
      $("#confirmacionesBtn").removeClass("btn-primary");
      $("#confirmacionesBtn").addClass("btn-secondary");
      $("#confirmacionesBtn").prop("disabled", true)
    })

    //colbac, nos suscribimos al evento "show.bs.modal" para que nos informe si se mostro el modal, el evento "show.bs.modal" pertenece a el componenete modal
    $("#Confirmacion").on("shown.bs.modal", function (e) {
      console.log("se mostro el modal")
    })

    //colbac, nos suscribimos al evento "hide.bs.modal" para que nos informe si se esta cerrondo el modal, el evento "show.bs.modal" pertenece a el componenete modal
    $("#Confirmacion").on("hide.bs.modal", function (e) {
      console.log("se esta cerrando  el modal")
    })

    //colbac, nos suscribimos al evento "hidden.bs.modal" para que nos informe si se cerro el modal, el evento "hidden.bs.modal" pertenece a el componenete modal
    $("#Confirmacion").on("hidden.bs.modal", function (e) {
      console.log("se cerro el modal")
      $("#confirmacionesBtn").prop("disabled", false)
      $("#confirmacionesBtn").removeClass("btn-secondary");
      $("#confirmacionesBtn").addClass("btn-primary");
    })

  })